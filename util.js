const fs = require('fs');
module.exports = {
	getRandomLine(file, { excludeEmpty = true, excludeStartsWith = [] } = {}) {
		const { floor, random } = Math;
		const lines = fs.readFileSync(file, 'utf8').split('\n');
		let randomLineOk = undefined;
		let randomLine = '';
		do {
			randomLineOk = true;
			randomLine = lines[floor(random() * lines.length)];
			const firstCharacter = randomLine.substring(0, 1);
			if (
				excludeEmpty &&
				randomLine.trim() === '' ||
				excludeStartsWith.includes(firstCharacter)
			) {
				randomLineOk = false;
			}
		} while (!randomLineOk);
		return randomLine;
	},
};
const sqlite = require('sqlite');
const config = require('../config.json');
const Axios = require('axios');
const Discord = require('discord.js');

class StreamRepositoryHelper {
	static async getUserIdsByChannelName(users) {
		const userInformation = await Axios.get(`https://api.twitch.tv/kraken/users?login=${users.join(',')}`,
			{
				headers: {
					'Client-ID': config.twitchClientId,
					'Accept': 'application/vnd.twitchtv.v5+json',
				},
			}
		);
		return userInformation.data.users.map(user => user['_id']);
	}
	static async getOnlineUsersByUserIds(userIds) {
		const queryString = `user_id=${userIds.join('&user_id=')}`;
		const userInformation = await Axios.get(`https://api.twitch.tv/helix/streams?${queryString}`,
			{
				headers: {
					'Client-ID': config.twitchClientId,
				},
			}
		);
		return userInformation.data.data.map(({ user_name }) => user_name.toLowerCase());
	}
}


class StreamAnnouncer {
	constructor(client) {
		this.client = client;
	}

	async init() {
		const data = await this.getData();
		const users = [];
		this.currentStatus = {};
		for (let i = 0; i < data.length; i++) {
			this.currentStatus[data[i].twitch_channel] = false;
			users.push(data[i].twitch_channel);
		}
		const twitchIds = await StreamRepositoryHelper.getUserIdsByChannelName(users);
		const onlineStatus = await StreamRepositoryHelper.getOnlineUsersByUserIds(twitchIds);
		for (let i = 0; i < onlineStatus.length; i++) {
			this.currentStatus[onlineStatus[i]] = true;
		}
		this.currentStatus = {};
		const channelNames = this.getChannelNamesFromData(data);
		for (let i = 0; i < channelNames.length; i++) {
			this.currentStatus[channelNames[i]] = false;
		}
	}

	getChannelNamesFromData(data) {
		return data.map(e => e.url.split('/').filter(e2 => e2.length > 0).slice(-1)[0]);
	}

	async announceNewStreams() {
		// check out (current) status
		const oldData = await this.getData();
		// find twitch IDs by channel name
		const twitchChannelNames = oldData.map(e => e.url.split('/').filter(e2 => e2.length > 0).slice(-1)[0]);
		const twitchIds = await StreamRepositoryHelper.getUserIdsByChannelName(twitchChannelNames);
		// check new status by twitch IDs
		const onlineStatus = await StreamRepositoryHelper.getOnlineUsersByUserIds(twitchIds);

		// check if some status has changed to online
		for (let i = 0; i < oldData.length; i++) {
			if (this.currentStatus[oldData[i].twitch_channel] === false && onlineStatus.includes(oldData[i].twitch_channel)) {
				this.currentStatus[oldData[i].twitch_channel] = true;
				// create embed
				this.createAndSendEmbed(oldData[i]);
			}
			else if (!onlineStatus.includes(oldData[i].twitch_channel)) {
				this.currentStatus[oldData[i].twitch_channel] = false;
			}
		}
	}

	async createAndSendEmbed(userInfo) {
		const {
			user_id: userId,
			url,
			channel_id: channelId,
		} = userInfo;
		const userNamePromise = await this.client.fetchUser(userId);
		const userName = this.client.channels.get(channelId).guild.members.get(userId).nickname || userNamePromise.username;


		const embed = new Discord.RichEmbed()
			.setColor('#00FF00')
			.setTitle(userName + ' started streaming!')
			.addField('Status', 'Online')
			.setDescription(url);
		this.client.channels.get(channelId).send(embed);
	}

	async getData() {
		this.lastStatus = {};
		const db = await sqlite.open(config.dbFile);
		const data = await db.all('SELECT user_id, url, channel_id FROM streams');
		for (let i = 0; i < data.length; i++) {
			data[i].twitch_channel = data[i].url.substr(data[i].url.lastIndexOf('/')).slice(1);
		}
		db.close();
		return data;
	}
}

module.exports = class {
	constructor(client) {
		if (!this.constructor.instance) {
			this.constructor.instance = new StreamAnnouncer(client);
		}
	}

	getInstance() {
		return this.constructor.instance;
	}

	async init() {
		this.constructor.instance.init();
	}
};
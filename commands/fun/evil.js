const request = require('request');
const config = require('../../config.json');
module.exports = class EvilCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'evil',
			group: 'fun',
			memberName: 'evil',
			description: 'Evil!',
			examples: ['evil <word>'],
			args: [
				{
					key: 'word',
					prompt: 'Please provide some text to put onto the image.',
					type: 'string',
				},
			],
		});
	}

	run(msg, { word }) {
		const url = 'https://api.imgflip.com/caption_image';
		request.post({
			url,
			form: {
				template_id: '40945639',
				username: config.imgflip.user,
				password: config.imgflip.pass,
				text1: word,
			},
		}, (err, response, body) => {
			if (err) {
				return msg.reply('Error retrieving image.');
			}
			const data = JSON.parse(body);
			const link = data.data.url;
			return msg.say(link);
		});
	}
};
module.exports = class ActivityCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'activity',
			group: 'fun',
			memberName: 'activity',
			description: 'Sets the activity of the bot. Can only be used twice per five minutes.',
			examples: ['activity <something>'],
			throttling: {
				usages: 2,
				duration: 600,
			},
			args: [
				{
					key: 'something',
					prompt: 'What do you want the activity to be set to?',
					type: 'string',
				},
			],
		});
	}

	run(msg, { something }) {
		msg.client.user.setActivity(something);
	}
};
const Discord = require('discord.js');
module.exports = class WtfCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'wtf',
			group: 'fun',
			memberName: 'wtf',
			description: 'Lets everybody know that something is really weird around here.',
			examples: ['wtf'],
		});
	}

	async run(msg) {
		const url = 'https://i.imgflip.com/1siyrl.jpg';
		const embed = new Discord.RichEmbed()
			.setColor('#0099FF')
			.setImage(url);
		if (msg.guild !== null && msg.guild.me.hasPermission('MANAGE_MESSAGES')) {
			msg.delete();
		}
		return msg.say(embed);
	}
};
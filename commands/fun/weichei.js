const util = require('../../util.js');
module.exports = class WeicheiCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'weichei',
			group: 'fun',
			memberName: 'weichei',
			description: 'Insults you or someone else.',
			examples: ['weichei', 'weichei <user>'],
			args: [
				{
					key: 'user',
					type: 'string',
					prompt: '',
					default: '',
				},
			],
		});
	}

	run(msg, { user }) {
		const weichei = util.getRandomLine('assets/weicheier.txt', {
			excludeEmpty: true,
			excludeStartsWith: ['#'],
		});
		if (user) {
			return msg.say(`${user}, ${weichei}`);
		}
		return msg.reply(weichei);
	}
};
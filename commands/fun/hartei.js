const util = require('../../util.js');
module.exports = class HarteiCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'hartei',
			group: 'fun',
			memberName: 'hartei',
			description: 'Compliments you or someone else.',
			examples: ['hartei', 'hartei <user>'],
			args: [
				{
					key: 'user',
					type: 'string',
					prompt: '',
					default: '',
				},
			],
		});
	}

	run(msg, { user }) {
		const hartei = util.getRandomLine('assets/harteier.txt', {
			excludeEmpty: true,
			excludeStartsWith: ['#'],
		});
		if (user) {
			return msg.say(`${user}, ${hartei}`);
		}
		return msg.reply(hartei);
	}
};
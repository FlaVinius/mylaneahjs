const yt = require('youtube-search');
const config = require('../../config.json');
module.exports = class SongifyCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'songify',
			group: 'fun',
			memberName: 'songify',
			examples: ['songify <keyword>'],
			description: 'Lets you search for Songify videos on YouTube',
			args: [
				{
					key: 'keyword',
					prompt: 'What do you want me to search for?',
					type: 'string',
				},
			],
		});
	}

	run(msg, { keyword }) {
		const options = {
			maxResults: 1,
			key: config.youtube,
		};
		yt(`songify ${keyword}`, options, (err, results) => {
			if (err) {
				return msg.reply('Error retrieving results.');
			}
			return msg.say(results[0].link);
		});
	}
};
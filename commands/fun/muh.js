module.exports = class MuhCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'muh',
			group: 'fun',
			memberName: 'muh',
			description: 'Moo!',
			examples: ['muh'],
		});
	}

	run(msg) {
		msg.say('Macht die Muh-Kuh!');
	}
};
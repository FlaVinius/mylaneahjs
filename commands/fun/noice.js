const Discord = require('discord.js');
module.exports = class NoiceCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'noice',
			aliases: ['nice'],
			group: 'fun',
			memberName: 'noice',
			description: 'Noice!',
			examples: ['noice'],
		});
	}

	async run(msg) {
		const url = 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQz6fTrCDrLZlpFuxXeX9HUpl8f4jIhr1az0_2k3iRjMY5Qpc6LLQ';
		const embed = new Discord.RichEmbed()
			.setColor('#0099FF')
			.setImage(url);
		if (msg.guild !== null && msg.guild.me.hasPermission('MANAGE_MESSAGES')) {
			msg.delete();
		}
		return msg.say(embed);
	}
};
const Discord = require('discord.js');
const slaps = [
	'https://media1.tenor.com/images/9ea4fb41d066737c0e3f2d626c13f230/tenor.gif?itemid=7355956',
	'https://media1.tenor.com/images/153b2f1bfd3c595c920ce60f1553c5f7/tenor.gif?itemid=10936993',
	'https://media1.tenor.com/images/fd14f63a93796ed26bd385c015df57b8/tenor.gif?itemid=4665506',
	'https://media1.tenor.com/images/b221fb3f50f0e15b3ace6a2b87ad0ffa/tenor.gif?itemid=8576304',
	'https://media1.tenor.com/images/d14969a21a96ec46f61770c50fccf24f/tenor.gif?itemid=5509136',
	'https://media1.tenor.com/images/b6d8a83eb652a30b95e87cf96a21e007/tenor.gif?itemid=10426943',
	'https://media1.tenor.com/images/4fa82be21ffd18c99a9708ba209d56ad/tenor.gif?itemid=5318916',
	'https://media1.tenor.com/images/5ab22ca640af20cd3b479694bde9e25c/tenor.gif?itemid=4961067',
	'https://media1.tenor.com/images/bf52b05fde72f946aa22ad36a44d3fa4/tenor.gif?itemid=7717421',
	'https://media1.tenor.com/images/047e78a99ba8c1ea4bfa6237d9f484aa/tenor.gif?itemid=8065158',
];

module.exports = class SlapCommand extends require('discord.js-commando').Command {

	constructor(client) {
		super(client, {
			name: 'slap',
			group: 'fun',
			memberName: 'slap',
			description: 'Slaps you or someone else.',
			examples: ['slap <user>'],
			args: [
				{
					key: 'user',
					type: 'string',
					prompt: '',
					default: '',
				},
			],
		});
	}

	run(msg, { user }) {
		const slap = slaps[Math.floor(Math.random() * slaps.length)];
		const embed = this.createEmbed(msg, user, slap);
		return msg.say(embed);
	}

	createEmbed(msg, user, slap) {
		let embed = undefined;
		if (!user || msg.mentions.everyone) {
			embed = new Discord.RichEmbed()
				.setColor('#ff393f')
				.setTitle(`${msg.author.username} slaps themselves.`)
				.setImage(slap);
		}
		else if (msg.mentions.members.size > 0) {
			embed = new Discord.RichEmbed()
				.setColor('#36ff3f')
				.setTitle(`${msg.author.username} slaps ${msg.mentions.members.first().user.username}`)
				.setImage(slap);
		}
		else {
			embed = new Discord.RichEmbed()
				.setColor('#36ff3f')
				.setTitle(`${msg.author.username} slaps ${msg.content.split(/ +/g)[1]}`)
				.setImage(slap);
		}
		return embed;
	}
};
module.exports = class BashCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'bash',
			group: 'utility',
			memberName: 'bash',
			description: 'Executes shell command.',
			examples: ['bash uptime'],
			ownerOnly: true,
			args: [
				{
					key: 'command',
					type: 'string',
					prompt: 'Which command would you like to run?',
				},
			],
		});
	}

	run(msg, { command }) {
		// eslint-disable-next-line no-unused-vars
		require('child_process').exec(command, (err, stdout, stderr) => {
			if (err) {
				console.error(err);
				return msg.reply('There was an error running this command.');
			}
			return msg.reply(stdout);
		});
	}
};
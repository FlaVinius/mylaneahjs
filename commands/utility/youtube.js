const yt = require('youtube-search');
const config = require('../../config.json');
module.exports = class YouTubeCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'youtube',
			aliases: ['yt'],
			group: 'utility',
			memberName: 'youtube',
			examples: ['youtube <keyword>'],
			description: 'Lets you search for YouTube videos.',
			args: [
				{
					key: 'keyword',
					prompt: 'What do you want me to search for?',
					type: 'string',
				},
			],
		});
	}

	run(msg, { keyword }) {
		const options = {
			maxResults: 1,
			key: config.youtube,
		};
		yt(keyword, options, (err, results) => {
			if (err) {
				return msg.reply('Error retrieving results.');
			}
			return msg.say(results[0].link);
		});
	}
};
module.exports = class UpdateCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'update',
			group: 'utility',
			memberName: 'update',
			ownerOnly: true,
			description: 'Downloads latest code files, runs npm install and exits the bot (should be restarted by pm2).',
		});
	}

	run(msg) {
		this.client.user.setStatus('dnd');
		require('simple-git')().pull(async (err, update) => {
			if (err) {
				console.log(err);
				return;
			}
			if (update && update.summary.changes) {
				const { changes, insertions, deletions } = update.summary;
				const reply = await msg.reply(`changes: ${changes}, insertions: ${insertions}, deletions: ${deletions}`);
				await reply.edit(`${reply.content}\nFiles updated. Running \`npm install\`.`);
				msg.channel.startTyping();
				require('child_process').exec('npm install', async (err, stdout, stderr) => { // eslint-disable-line no-unused-vars
					if (err) {
						console.error(err);
						return;
					}
					await reply.edit(`${reply.content}\n\`npm install\` done.`);
					msg.channel.stopTyping();
					process.exit(1);
				});
				return;
			}
			else {
				this.client.user.setStatus('online');
				return msg.reply('Already up-to-date.');
			}
		});
	}
};
module.exports = class ImageCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'image',
			group: 'utility',
			memberName: 'image',
			description: 'Links to google images with specified keyword(s).',
			examples: ['image <text>'],
			args: [
				{
					key: 'text',
					prompt: 'What would you like me to find images of?',
					type: 'string',
				},
			],
		});
	}

	run(msg, { text }) {
		text = encodeURI(text);
		const url = `https://www.google.de/search?q=${text}&tbm=isch`;
		msg.say(url);
	}
};
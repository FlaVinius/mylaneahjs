const google = require('google');
module.exports = class GoogleCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'google',
			group: 'utility',
			memberName: 'google',
			examples: ['google <keyword>'],
			description: 'Lets you google something',
			args: [
				{
					key: 'keyword',
					prompt: 'What do you want me to google?',
					type: 'string',
				},
			],
		});
	}

	run(msg, { keyword }) {
		google.resultsPerPage = 25;
		google(keyword, (err, res) => {
			if (err) {
				return msg.reply('Error retrieving results.');
			}
			let i = 0;
			let hardLimit = 10;
			while (i < Math.min(3, res.links.length) && hardLimit > 0) {
				if (res.links[i].href) {
					console.log(res.links[i].href);
					msg.say(res.links[i].href);
					i++;
				}
				hardLimit--;
			}
		});
	}
};

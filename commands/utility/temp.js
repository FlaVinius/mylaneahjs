const request = require('request');
module.exports = class TempCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'temp',
			group: 'utility',
			memberName: 'temp',
			description: 'Gets the room temperature.',
			examples: ['temp'],
			ownerOnly: true,
		});
	}

	run(msg) {
		request('http://fishberrypi/aqua/temp.php?type=room', { timeout: 2000 }, (err, response, body) => {
			if (err) {
				return msg.reply('Error retrieving temperature.');
			}
			return msg.say(`Zimmertemperatur: ${body}°C`);
		});
	}
};
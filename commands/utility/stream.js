const sqlite = require('sqlite');
const Discord = require('discord.js');
const Axios = require('axios');
const config = require('../../config.json');
const dateFormat = require('date-format');
const StreamAnnouncer = require('../../repositories/streamannouncer');
module.exports = class StreamCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'stream',
			group: 'utility',
			memberName: 'stream',
			description: 'Let\'s you set your twitch stream link or gets someone\'s twitch stream link when you `mention` someone.',
			examples: [
				`\`${config.prefix}stream\` - shows a link to your stream if set`,
				`\`${config.prefix}stream set https://twitch.tv/yournamehere\` - sets your stream to the specified Twitch URL`,
				`\`${config.prefix}stream @mention\` - gets someone's stream URL and status`,
			],
			throttling: {
				usages: 3,
				duration: 60,
			},
			format: '[set <Twitch URL>] | [@mention]',
			argsPromptLimit: 0,
			guildOnly: true,
			args: [
				{
					key: 'setOrUser',
					type: 'member|string',
					prompt: '',
					default: '',
					error: [
						'The first argument must be `set` (followed by a Twitch URL) or a `@mention`.',
						'Accepted URL examples:',
						'`https://twitch.tv/yournamehere`',
						'`https://www.twitch.tv/yournamehere`',
					].join('\n'),
					validate: val => {
						const args = String(val).split(/\s+/g);
						const firstArgumentIsEmpty = args[0] === '';
						const firstArgumentIsMention = typeof this.getUserFromMention(client, args[0]) === 'object';
						const firstArgumentIsSet = args[0] === 'set';
						const secondArgumentIsTwitchUrl = /^https?:\/\/(www\.)?twitch\.tv\/[A-Za-z0-9]+\/?$/.test(args[1]);
						return (
							firstArgumentIsEmpty ||
							firstArgumentIsMention ||
							firstArgumentIsSet &&
								secondArgumentIsTwitchUrl
						);
					},
				},
			],
		});
	}

	async run(msg, { setOrUser }) {
		if (setOrUser === '') {
			setOrUser = msg.member;
		}
		const isUser = typeof setOrUser === 'object';
		if (isUser) {
			return await this.handleArgIsUser(msg, setOrUser);
		}
		const args = setOrUser.split(/\s+/g);
		if (args[0] === 'set') {
			return await this.setUrl(msg, args[1]);
		}
	}

	getUserFromMention(client, mention) {
		if (typeof mention === 'string') {
			if (mention.startsWith('<@') && mention.endsWith('>')) {
				mention = mention.slice(2, -1);
				if (mention.startsWith('!')) {
					mention = mention.slice(1);
				}
				return client.users.get(mention);
			}
		}
	}

	async handleArgIsUser(msg, linkOrUser) {
		const db = await sqlite.open(config.dbFile);
		const userId = linkOrUser.user.id;
		try {
			const stream = await db.get('SELECT url FROM streams WHERE user_id = ?', userId);
			if (typeof stream === 'undefined') {
				return msg.reply(`No stream URL found for ${linkOrUser}.`);
			}
			else {
				const user = stream.url.split('/').filter(e => e.length > 0).slice(-1)[0];
				const result = await Axios.get(
					`https://api.twitch.tv/helix/streams?user_login=${user}`,
					{
						headers: {
							'Client-ID': config.twitchClientId,
						},
					}
				);
				const embed = this.createEmbed(linkOrUser, result, stream);
				return msg.say(embed);
			}
		}
		catch (err) {
			console.log(err);
			return msg.reply('There was an error reading your stream URL.');
		}
		finally {
			db.close();
		}
	}

	async setUrl(msg, url) {
		const db = await sqlite.open(config.dbFile);
		const userId = msg.author.id;
		const channelId = msg.channel.id;
		try {
			await db.run('DELETE FROM streams WHERE user_id = ?', userId);
			await db.run('INSERT INTO streams (user_id, url, channel_id) VALUES (?, ?, ?)', userId, url, channelId);
			return msg.reply(`Your stream URL has been set to ${url}.`);
		}
		catch (err) {
			return msg.reply('There was an error setting your stream URL.');
		}
		finally {
			db.close();
		}
	}

	createEmbed(linkOrUser, result, stream) {
		const userIsStreaming = result.data.data.length > 0;
		const embed = new Discord.RichEmbed()
			.setColor(userIsStreaming ? '#00FF00' : '#FF0000')
			.setTitle('Twitch Stream for ' + (linkOrUser.nickname || linkOrUser.user.username))
			.addField('Status', (userIsStreaming ? 'Online' : 'Offline'))
			.setDescription(stream.url);
		if (userIsStreaming) {
			const startedAt = dateFormat('dd/MM/yyyy hh:mm', new Date(result.data.data[0].started_at));
			embed.addField('Since (dd/mm/yyyy hh:mm)', startedAt);
		}
		const streamAnnouncer = new StreamAnnouncer(this.client).getInstance();
		const twitchChannel = streamAnnouncer.getChannelNamesFromData([stream])[0].toLowerCase();
		streamAnnouncer[twitchChannel] = userIsStreaming;
		return embed;
	}
};
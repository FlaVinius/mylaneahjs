const moment = require('moment');
const sqlite = require('sqlite');
const { dbFile } = require('../../config');
module.exports = class SeenCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'seen',
			group: 'utility',
			memberName: 'seen',
			description: 'Tells you when the mentioned user was last seen having a presence status other than offline',
			examples: [
				'seen @mention',
			],
			args: [
				{
					key: 'mention',
					type: 'member',
					prompt: 'Please provide a user (mention).',
				},
			],
		});
	}

	async run(msg, { mention }) {
		console.log(mention);
		const userId = mention.user.id;
		const currentStatus = mention.user.presence.status;
		if (currentStatus !== 'offline') {
			const appendix = currentStatus === 'online' ? '' : ` (but ${currentStatus})`;
			return msg.reply(`${mention} is here with us right now${appendix}!`);
		}
		const db = await sqlite.open(dbFile);
		const timestamp = await db.get('SELECT timestamp FROM seen WHERE user_id = ?', userId);
		if (typeof timestamp === 'undefined') {
			return msg.reply(`I have never seen ${mention} going offline.`);
		}
		else {
			const dateTime = moment(timestamp.timestamp * 1000);
			const humanDate = dateTime.format('DD.MM.YYYY');
			const humanTime = dateTime.format('HH:mm:ss');
			return msg.reply(`${mention} was last seen going offline on ${humanDate} at ${humanTime}.`);
		}
	}
};
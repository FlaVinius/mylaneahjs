const request = require('request');
const moment = require('moment');
const Discord = require('discord.js');
const config = require('../../config.json');
module.exports = class WetterCommand extends require('discord.js-commando').Command {
	constructor(client) {
		super(client, {
			name: 'wetter',
			group: 'utility',
			memberName: 'wetter',
			description: 'Gets weather information of specified location.',
			examples: ['wetter <city>'],
			args: [
				{
					key: 'city',
					prompt: 'Please provide a location.',
					type: 'string',
				},
			],
		});
		this.translations = {
			'clear sky': 'Klarer Himmel',
			'few clouds': 'Ein paar Wolken',
			'scattered clouds': 'Vereinzelte Wolken',
			'broken clouds': 'Stark bewölkt',
			'overcast clouds': 'Bedeckt',
			'shower rain': 'Regenschauer',
			'rain': 'Regen',
			'thunderstorm': 'Gewitter',
			'snow': 'Schnee',
			'mist': 'Nebel',
			'clouds': 'Wolkig',
			'light rain': 'Leichter Regen',
			'moderate rain': 'Mäßiger Regen',
			'light intensity drizzle rain': 'Leichter Nieselregen',
		};
	}

	run(msg, { city }) {
		const url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&APPID=${config.owm}&units=metric`;
		request(url, (err, response, body) => {
			if (err) {
				return msg.reply('Error retrieving weather.');
			}
			const weatherData = JSON.parse(body);
			const location = weatherData.name;
			if (weatherData.cod !== 200) {
				return msg.reply('Location not found.');
			}
			const weatherDescription = weatherData.weather.length && this.translations[weatherData.weather[0].description.toLowerCase()] || weatherData.weather[0].description;
			const weatherIcon = `http://openweathermap.org/img/w/${weatherData.weather[0].icon}.png`;
			const windSpeed = (weatherData.wind.speed * 3.6).toLocaleString().replace('.', ',');
			const temp = weatherData.main.temp.toLocaleString().replace('.', ',');
			const dateTime = moment.unix(weatherData.dt).format('DD.MM.YYYY HH:mm');
			const embed = new Discord.RichEmbed()
				.setColor('#00FF00')
				.setThumbnail(weatherIcon)
				.setTitle(`Wetter in ${location}`)
				.setDescription(`Stand: ${dateTime} Uhr`)
				.addField('Temperatur', `${temp} °C`, true)
				.addField('Windgeschwindigkeit', `${windSpeed} km/h`, true)
				.addField('Status', weatherDescription);
			return msg.say(embed);
		});
	}
};
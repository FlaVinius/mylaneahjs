const path = require('path');
const Commando = require('discord.js-commando');
const { prefix, token, dbFile } = require('./config');
const sqlite = require('sqlite');
const util = require('./util');
const schedule = require('node-schedule');
const client = new Commando.Client({
	commandPrefix: prefix,
	owner: '421772248987336704',
	disableEveryone: true,
	unknownCommandResponse: false,
});
const StreamAnnouncer = require('./repositories/streamannouncer');

/** probability for deine Mutter jokes */
const initialProbability = 0.1;
let probability = initialProbability;

client.registry
	.registerGroups([
		['fun', 'Weicheier, Harteier'],
		['utility', 'Temperatur, Wetter'],
	])
	.registerDefaults()
	.registerCommandsIn(path.join(__dirname, 'commands'));

client.setProvider(
	sqlite.open(path.join(__dirname, dbFile))
		.then(db => new Commando.SQLiteProvider(db))
).catch(console.error);

client.on('ready', async () => {
	console.log('Logged in!');
	const db = await sqlite.open(dbFile);
	await Promise.all([
		db.exec('CREATE TABLE IF NOT EXISTS streams (user_id TEXT, url TEXT, channel_id TEXT)'),
		db.exec('CREATE TABLE IF NOT EXISTS seen (user_id TEXT, timestamp INT)'),
	]);
	db.close();

	const rule = new schedule.RecurrenceRule();
	const date = new Date();
	rule.minute = date.getMinutes();
	rule.hour = date.getHours();
	const offset = 5;
	schedule.scheduleJob(rule, function() {
		client.user.setActivity(util.getRandomLine('assets/activities.txt'));
		rule.hour = (rule.hour + 1) % 24;
		rule.minute = Math.abs(Math.floor(Math.random() * ((rule.minute + offset) - (rule.minute - offset) + 1) + rule.minute - offset) % 60);
		this.reschedule(rule);
	}).invoke();

	const streamAnnouncer = new StreamAnnouncer(client).getInstance();
	await streamAnnouncer.init();
	// TODO: Do not trigger announce, when !stream command has been used before and was online
	schedule.scheduleJob('*/1 * * * *', async function() {
		streamAnnouncer.announceNewStreams();
	});
});


client.on('presenceUpdate', async (oldMember, newMember) => {
	const userId = oldMember.user.id;
	const oldStatus = oldMember.presence.status;
	const newStatus = newMember.presence.status;
	if (oldStatus !== 'offline' && newStatus === 'offline') {
		const db = await sqlite.open(dbFile);
		await db.run('DELETE FROM seen WHERE user_id = ?', userId);
		await db.run('INSERT INTO seen (user_id, timestamp) VALUES (?, ?)', userId, Math.floor(Date.now() / 1000));
		db.close();
	}
});

client.on('message', message => {
	const messageSplit = message.content.split(/ +/);
	const cmd = messageSplit[0].toLowerCase();

	if (!message.author.bot && /mu(dda|dder|ddi|tta|tter|tti)/i.test(message.content)) {
		if (Math.random() <= probability) {
			message.reply(util.getRandomLine('assets/deine_mutter.txt'));
			probability = initialProbability;
		}
		else {
			probability += 0.05;
		}
	}
	if (cmd === 'ag' && messageSplit.length === 1) {
		message.channel.send('Ag, ag ag ag ag ag AG AG AG!');
	}
});

client.on('error', console.error);
client.on('warn', console.warn);
client.on('debug', console.debug);
client.on('disconnect', () => {
	console.warn('Disconnecting');
	process.exit(1);
});
client.on('reconnecting', () => console.warn('Reconnecting'));
client.on('commanderror', (cmd, err) => console.error(`Error in command ${cmd.groupID}:${cmd.memberName}`, err));

client.login(token);

process.on('unhandledRejection', function(reason, p) {
	console.log('unhandled at ', p, ' reason: ', reason);
});